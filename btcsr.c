/*
 *  Copyright (C) 2004-2010  Marcel Holtmann <marcel@holtmann.org>
 *  Copyright (C) 2019  Collabora Ltd
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <errno.h>
#include <stdlib.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>

#include "btcsr.h"

#define CSR_STORES_PSI		(0x0001)
#define CSR_STORES_PSF		(0x0002)
#define CSR_STORES_PSROM	(0x0004)
#define CSR_STORES_PSRAM	(0x0008)

#define CSR_VARID_COLD_RESET				0x4001	/* valueless */
#define CSR_VARID_WARM_RESET				0x4002	/* valueless */
#define CSR_VARID_COLD_HALT					0x4003	/* valueless */
#define CSR_VARID_WARM_HALT					0x4004	/* valueless */
#define CSR_VARID_PS						0x7003	/* complex */

#define CSR_PSKEY_LC_MAX_TX_POWER			0x0017	/* int16 */
#define CSR_PSKEY_LC_DEFAULT_TX_POWER		0x0021	/* int16 */
#define CSR_PSKEY_LC_MAX_TX_POWER_NO_RSSI	0x002d	/* int8 */

static uint16_t seqnum = 0x0000;

static int csr_do_command(int dd, uint16_t command, uint16_t seqnum, uint16_t varid, uint8_t *value, uint16_t length)
{
	unsigned char cp[254], rp[254];
	struct hci_request rq;
	uint8_t cmd[10];
	uint16_t size;

	size = (length < 8) ? 9 : ((length + 1) / 2) + 5;

	cmd[0] = command & 0xff;
	cmd[1] = command >> 8;
	cmd[2] = size & 0xff;
	cmd[3] = size >> 8;
	cmd[4] = seqnum & 0xff;
	cmd[5] = seqnum >> 8;
	cmd[6] = varid & 0xff;
	cmd[7] = varid >> 8;
	cmd[8] = 0x00;
	cmd[9] = 0x00;

	memset(cp, 0, sizeof(cp));
	cp[0] = 0xc2;
	memcpy(cp + 1, cmd, sizeof(cmd));
	memcpy(cp + 11, value, length);

	switch (varid) {
	case CSR_VARID_COLD_RESET:
	case CSR_VARID_WARM_RESET:
	case CSR_VARID_COLD_HALT:
	case CSR_VARID_WARM_HALT:
		return hci_send_cmd(dd, OGF_VENDOR_CMD, 0x00, (size * 2) + 1, cp);
	}

	memset(&rq, 0, sizeof(rq));
	rq.ogf    = OGF_VENDOR_CMD;
	rq.ocf    = 0x00;
	rq.event  = EVT_VENDOR;
	rq.cparam = cp;
	rq.clen   = (size * 2) + 1;
	rq.rparam = rp;
	rq.rlen   = sizeof(rp);

	if (hci_send_req(dd, &rq, 2000) < 0)
		return -1;

	if (rp[0] != 0xc2) {
		errno = EIO;
		return -1;
	}

	if ((rp[9] + (rp[10] << 8)) != 0) {
		errno = ENXIO;
		return -1;
	}

	memcpy(value, rp + 11, length);

	return 0;
}

static int csr_read_hci(int dd, uint16_t varid, uint8_t *value, uint16_t length)
{
	return csr_do_command(dd, 0x0000, seqnum++, varid, value, length);
}

static int csr_write_hci(int dd, uint16_t varid, uint8_t *value, uint16_t length)
{
	return csr_do_command(dd, 0x0002, seqnum++, varid, value, length);
}

static int csr_check(int dd) {
	struct hci_version ver;
	int err;

	if ((err = hci_read_local_version(dd, &ver, 1000)) < 0)
		return err;

	if (ver.manufacturer != 10) {
		errno = ENODEV;
		return -1;
	}

	return 0;
}

static uint16_t csr_txpower_to_key(BtCsrTxPower entry) {
	switch (entry) {
		case BTCSR_MAX_TX_POWER:
			return CSR_PSKEY_LC_MAX_TX_POWER;
		case BTCSR_DEFAULT_TX_POWER:
			return CSR_PSKEY_LC_DEFAULT_TX_POWER;
		case BTCSR_MAX_TX_POWER_NO_RSSI:
			return CSR_PSKEY_LC_MAX_TX_POWER_NO_RSSI;
		default:
			/* Should never happen */
			exit(1);
	}
}

/* btcsr_get_tx_power
 *   dd    - Device descriptor returned by hci_open_dev
 *   entry - BtCsrTxPower entry to get from PStore
 *   level - return TX power level
 *
 * Read level from Default storage (i.e. psram (RAM), psi (implementation),
 * psf (factory) or psrom (ROM), in this order) for one the CSR PSKey related
 * to TX Power.
 *
 * returns 0 on success, in case of failure errno is set.
 */
int btcsr_get_tx_power(int dd, BtCsrTxPower entry, int16_t *level)
{
	uint16_t key = csr_txpower_to_key(entry);
	uint8_t array[8];
	int err;

	if ((err = csr_check(dd)) < 0)
		return err;

	memset(array, 0, sizeof(array));
	array[0] = key & 0xff;
	array[1] = key >> 8;
	array[2] = 1;	/* 1 word length */
	array[3] = 0;
	array[4] = 0;	/* use default store */
	array[5] = 0;

	if ((err = csr_read_hci(dd, CSR_VARID_PS, array, sizeof(array))) < 0)
		return err;

	*level = array[6] | (array[7] << 8);

	return 0;
}

/* btcsr_set_tx_power
 *   dd    - Device descriptor returned by hci_open_dev
 *   entry - BtCsrTxPower entry to set to PStore
 *   level - TX power level
 *
 * Write level to RAM storage for one the CSR PSKey related to TX Power.
 * This needs a warm reset to be used by the chipset.
 * Cold reset or reboot reverts to TX Power level value stored in long term
 * storages, i.e. psi (implementation), psf (factory) or psrom (ROM).
 * This can only work with CSR chipsets.
 *
 * returns 0 on success, in case of failure errno is set.
 */
int btcsr_set_tx_power(int dd, BtCsrTxPower entry, int16_t level)
{
	uint16_t key = csr_txpower_to_key(entry);
	uint8_t array[8];
	int err;

	if ((err = csr_check(dd)) < 0)
		return err;

	memset(array, 0, sizeof(array));
	array[0] = key & 0xff;
	array[1] = key >> 8;
	array[2] = 1;	/* 1 word length */
	array[3] = 0;
	array[4] = CSR_STORES_PSRAM & 0xff;
	array[5] = CSR_STORES_PSRAM >> 8;
	array[6] = level & 0xff;
	array[7] = level >> 8;

	return csr_write_hci(dd, CSR_VARID_PS, array, sizeof(array));
}

/* btcsr_warm_reset
 *   dd - Device descriptor returned by hci_open_dev
 *
 * Warm reset the dd device.
 * This can only work with CSR chipsets.
 *
 * returns 0 on success, in case of failure errno is set.
 */
int btcsr_warm_reset(int dd) {
	int err;

	if ((err = csr_check(dd)) < 0)
		return err;

	return csr_write_hci(dd, CSR_VARID_WARM_RESET, NULL, 0);
}
