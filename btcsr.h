/*
 *  Copyright (C) 2019  Collabora Ltd
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef __BTCSR_H
#define __BTCSR_H

#ifdef __cplusplus
extern "C" {
#endif

/* BtCsrTxPower:
 * BTCSR_MAX_TX_POWER: 'Maximum TX Power' used during a connection
 * BTCSR_DEFAULT_TX_POWER: 'Default TX Power' used during inquiries and page
 *    scans
 * BTCSR_MAX_TX_POWER_NO_RSSI: 'Maximun TX Power No RSSI' used if the remote
 *    device does not support power control messaging.
 */
typedef enum {
    BTCSR_MAX_TX_POWER = 0,
    BTCSR_DEFAULT_TX_POWER,
    BTCSR_MAX_TX_POWER_NO_RSSI
} BtCsrTxPower;

int btcsr_get_tx_power(int dd, BtCsrTxPower entry, int16_t *level);
int btcsr_set_tx_power(int dd, BtCsrTxPower entry, int16_t level);
int btcsr_warm_reset(int dd);

#ifdef __cplusplus
}
#endif

#endif /* __BTCSR_H */
