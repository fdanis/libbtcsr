# Helper Library for CSR specific commands

## Build

```
cmake -H. -Bbuild -DCMAKE_INSTALL_PREFIX:PATH=/tmp/foo
cd build
make
```

This will build `libbtcsr.so` library and `btcsrtest` application to exercise libbtcsr and BlueZ functions related to RSSI, Tx Power, Link Quality and AFH.

## Test app

```
$ ./btcsrtest -h
Usage:
    btcsrtest [options] <command> [command parameters]
Options:
    --help	Display help
    -i dev	HCI device
Commands:
    csr-get-max-tx    Get Maximun TX Power of CSR chipset
    csr-set-max-tx    Set Maximun TX Power of CSR chipset
    csr-warmreset     Warm reset of CSR chipset
    conn-info         Get connection information
    set-afh           Set AFH Host Channel Classification map

For more information on the usage of each command use:
	btcsrtest <command> --help
```

HCI device to use is in same form as in _hciconfig_.</br>
`csr-*` commands are CSR specific.</br>
`conn-info` only does not need to be run as root.

`csr-get-max-tx` and `warmreset` do not take arguments.</br>
`csr-set-max-tx` takes the value to apply expressed in dBm.</br>
`conn-info` takes the remote device Bluetooth MAC address as parameter.</br>
`set-afh` takes 10 bytes representing the _AFH Host Channel Classification_ map to apply.</br>

For example, to request second HCI device to set _AFH Host Channel Classification_ map with channels 0-23 and 47-78:
```
$ ./btcsrtest -i hci1 set-afh 0xff 0xff 0xff 0x00 0x00 0x80 0xff 0xff 0xff 0x7f
```
