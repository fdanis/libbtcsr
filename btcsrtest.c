/*
 *  Copyright (C) 2000-2001  Qualcomm Incorporated
 *  Copyright (C) 2002-2003  Maxim Krasnyansky <maxk@qualcomm.com>
 *  Copyright (C) 2004-2010  Marcel Holtmann <marcel@holtmann.org>
 *  Copyright (C) 2019  Collabora Ltd
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <getopt.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>

#include "btcsr.h"

#define for_each_opt(opt, long, short) while ((opt=getopt_long(argc, argv, short ? short:"+", long, NULL)) != -1)

static void helper_arg(int min_num_arg, int max_num_arg, int *argc,
			char ***argv, const char *usage)
{
	*argc -= optind;
	/* too many arguments, but when "max_num_arg < min_num_arg" then no
		 limiting (prefer "max_num_arg=-1" to gen infinity)
	*/
	if ( (*argc > max_num_arg) && (max_num_arg >= min_num_arg ) ) {
		fprintf(stderr, "%s: too many arguments (maximal: %i)\n",
				*argv[0], max_num_arg);
		printf("%s", usage);
		exit(1);
	}

	/* print usage */
	if (*argc < min_num_arg) {
		fprintf(stderr, "%s: too few arguments (minimal: %i)\n",
				*argv[0], min_num_arg);
		printf("%s", usage);
		exit(0);
	}

	*argv += optind;
}

/* Get Maximun TX Power for CSR chipset */

static struct option get_max_tx_options[] = {
	{ "help",	0, 0, 'h' },
	{ 0, 0, 0, 0 }
};

static const char *get_max_tx_help =
	"Usage:\n"
	"\tcsr-get-max-tx\n";

static void cmd_get_max_tx(int dev_id, int argc, char **argv)
{
	int opt, dd;
	int16_t level;

	for_each_opt(opt, get_max_tx_options, NULL) {
		switch (opt) {
		default:
			printf("%s", get_max_tx_help);
			return;
		}
	}
	helper_arg(0, 0, &argc, &argv, get_max_tx_help);

	dd = hci_open_dev(dev_id);
	if (dd < 0) {
		perror("HCI device open failed");
		exit(1);
	}

	if (btcsr_get_tx_power(dd, BTCSR_MAX_TX_POWER, &level) < 0) {
		perror("Failed to get Max TX power");
		exit(1);
	}
	printf("Max TX power: %d\n", level);

	if (btcsr_get_tx_power(dd, BTCSR_DEFAULT_TX_POWER, &level) < 0) {
		perror("Failed to get Default TX power");
		exit(1);
	}
	printf("Default TX power: %d\n", level);

	if (btcsr_get_tx_power(dd, BTCSR_MAX_TX_POWER_NO_RSSI, &level) < 0) {
		perror("Failed to get Max TX power no RSSI");
		exit(1);
	}
	printf("Max TX power no RSSI: %d\n", level);

	hci_close_dev(dd);
}

/* Set Maximun TX Power for CSR chipset */

static struct option set_max_tx_options[] = {
	{ "help",	0, 0, 'h' },
	{ 0, 0, 0, 0 }
};

static const char *set_max_tx_help =
	"Usage:\n"
	"\tcsr-set-max-tx <value>\n";

static void cmd_set_max_tx(int dev_id, int argc, char **argv)
{
	int opt, dd;
	struct hci_version ver;
	int16_t value;

	for_each_opt(opt, set_max_tx_options, NULL) {
		switch (opt) {
		default:
			printf("%s", set_max_tx_help);
			return;
		}
	}
	helper_arg(1, 1, &argc, &argv, set_max_tx_help);

	dd = hci_open_dev(dev_id);
	if (dd < 0) {
		perror("HCI device open failed");
		exit(1);
	}

	if (!strncasecmp(argv[0], "0x", 2))
		value = strtol(argv[0] + 2, NULL, 16);
	else
		value = atoi(argv[0]);

	if (btcsr_set_tx_power(dd, BTCSR_MAX_TX_POWER, value) < 0) {
		perror("Failed to set Max TX power");
		exit(1);
	}

	hci_close_dev(dd);
}

/* Warm reset for CSR chipset */

static struct option warm_reset_options[] = {
	{ "help",	0, 0, 'h' },
	{ 0, 0, 0, 0 }
};

static const char *warm_reset_help =
	"Usage:\n"
	"\tcsr-warmreset\n";

static void cmd_warmreset(int dev_id, int argc, char **argv)
{
	int opt, dd;

	for_each_opt(opt, warm_reset_options, NULL) {
		switch (opt) {
		default:
			printf("%s", warm_reset_help);
			return;
		}
	}
	helper_arg(0, 0, &argc, &argv, warm_reset_help);

	dd = hci_open_dev(dev_id);
	if (dd < 0) {
		perror("HCI device open failed");
		exit(1);
	}

	if (btcsr_warm_reset(dd) < 0) {
		perror("Failed to Warm reset CSR chipset");
		exit(1);
	}

	hci_close_dev(dd);
}

/* Info about remote connection */

static struct option conn_info_options[] = {
	{ "help",	0, 0, 'h' },
	{ 0, 0, 0, 0 }
};

static const char *conn_info_help =
	"Usage:\n"
	"\tconn-info <bdaddr>\n";

static void cmd_conn_info(int dev_id, int argc, char **argv)
{
	int opt, dd;
	bdaddr_t bdaddr;
	struct hci_conn_info_req *cr;
	uint16_t handle;
	int8_t rssi, tx, max_tx;
	uint8_t lq;
	uint8_t mode, map[10];

	for_each_opt(opt, conn_info_options, NULL) {
		switch (opt) {
		default:
			printf("%s", conn_info_help);
			return;
		}
	}
	helper_arg(1, 1, &argc, &argv, conn_info_help);

	str2ba(argv[0], &bdaddr);

	dd = hci_open_dev(dev_id);
	if (dd < 0) {
		perror("HCI device open failed");
		exit(1);
	}

	cr = malloc(sizeof(*cr) + sizeof(struct hci_conn_info));
	if (!cr) {
		perror("Can't get connection info");
		close(dd);
		exit(1);
	}

	bacpy(&cr->bdaddr, &bdaddr);
	cr->type = ACL_LINK;
	if (ioctl(dd, HCIGETCONNINFO, (unsigned long) cr) < 0) {
		perror("Get connection info failed");
		exit(1);
	}

	handle = htobs(cr->conn_info->handle);

	free(cr);

	if (hci_read_rssi(dd, handle, &rssi, 1000) < 0) {
		printf("Read RSSI failed");
		exit(1);
	}

	if (hci_read_link_quality(dd, handle, &lq, 1000) < 0) {
		printf("Read Link Quality failed");
		exit(1);
	}

	if (hci_read_transmit_power_level(dd, handle, 0, &tx, 1000) < 0) {
		printf("Read Current transmit power level failed");
		exit(1);
	}

	if (hci_read_transmit_power_level(dd, handle, 1, &max_tx, 1000) < 0) {
		printf("Read Maximum transmit power level failed");
		exit(1);
	}

	if (hci_read_afh_map(dd, handle, &mode, map, 1000) < 0) {
		perror("HCI read AFH map request failed");
		exit(1);
	}

	printf("RSSI:         %d\n", rssi);
	printf("Link Quality: %d\n", lq);
	printf("TX power:     %d\n", tx);
	printf("Max TX power: %d\n", max_tx);
	if (mode == 0x01) {
		int i;
		printf("AFH map: 0x");
		for (i = 0; i < 10; i++)
			printf("%02x", map[i]);
		printf("\n");
	} else
		printf("AFH disabled\n");

	hci_close_dev(dd);
}

/* Set AFH Host Channel Classification map */

static struct option set_afh_options[] = {
	{ "help",	0, 0, 'h' },
	{ 0, 0, 0, 0 }
};

static const char *set_afh_help =
	"Usage:\n"
	"\tset-afh <10 bytes AFH map>\n"
	"Example:\n"
	"\tset-afh 0xff 0xff 0xff 0xff 0xff 0xff 0xff 0xff 0xff 0x7f\n";

static void cmd_set_afh(int dev_id, int argc, char **argv)
{
	int opt, dd, i, len;
	bdaddr_t bdaddr;
	uint8_t map[10];

	for_each_opt(opt, set_afh_options, NULL) {
		switch (opt) {
		default:
			printf("%s", set_afh_help);
			return;
		}
	}
	helper_arg(10, 10, &argc, &argv, set_afh_help);

	for (i = 0, len = 0; i < argc && len < (int) sizeof(map); i++, len++)
		map[i] = (uint8_t) strtol(argv[i], NULL, 16);

	dd = hci_open_dev(dev_id);
	if (dd < 0) {
		perror("HCI device open failed");
		exit(1);
	}

	if (hci_set_afh_classification(dd, map, 1000) < 0) {
		perror("HCI read AFH map request failed");
		exit(1);
	}

	hci_close_dev(dd);
}

static struct {
	char *cmd;
	void (*func)(int dev_id, int argc, char **argv);
	char *doc;
} command[] = {
	{ "csr-get-max-tx", cmd_get_max_tx,    "Get Maximun TX Power of CSR chipset"},
	{ "csr-set-max-tx", cmd_set_max_tx,    "Set Maximun TX Power of CSR chipset"},
	{ "csr-warmreset",  cmd_warmreset,     "Warm reset of CSR chipset"},
	{ "conn-info",      cmd_conn_info,     "Get connection information"},
	{ "set-afh",        cmd_set_afh,       "Set AFH Host Channel Classification map"},
	{ NULL, NULL, 0 }
};

static void usage(void)
{
	int i;

	printf("Usage:\n"
		"\tbtcsrtest [options] <command> [command parameters]\n");
	printf("Options:\n"
		"\t--help\tDisplay help\n"
		"\t-i dev\tHCI device\n");
	printf("Commands:\n");
	for (i = 0; command[i].cmd; i++)
		printf("\t%-4s\t%s\n", command[i].cmd,
		command[i].doc);
	printf("\n"
		"For more information on the usage of each command use:\n"
		"\tbtcsrtest <command> --help\n" );
}

static struct option main_options[] = {
	{ "help",	0, 0, 'h' },
	{ "device",	1, 0, 'i' },
	{ 0, 0, 0, 0 }
};

int main(int argc, char *argv[])
{
	int opt, i, dev_id = 0;
	bdaddr_t ba;

	while ((opt=getopt_long(argc, argv, "+i:h", main_options, NULL)) != -1) {
		switch (opt) {
		case 'i':
			dev_id = hci_devid(optarg);
			if (dev_id < 0) {
				perror("Invalid device");
				exit(1);
			}
			break;

		case 'h':
		default:
			usage();
			exit(0);
		}
	}

	argc -= optind;
	argv += optind;
	optind = 0;

	if (argc < 1) {
		usage();
		exit(0);
	}

	if (hci_devba(dev_id, &ba) < 0) {
		perror("Device is not available");
		exit(1);
	}

	for (i = 0; command[i].cmd; i++) {
		if (strncmp(command[i].cmd,
				argv[0], strlen(command[i].cmd)))
			continue;

		command[i].func(dev_id, argc, argv);
		break;
	}

	if (command[i].cmd == 0) {
		fprintf(stderr, "Unknown command - \"%s\"\n", *argv);
		exit(1);
	}

	return 0;
}
